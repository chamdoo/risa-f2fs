/**
 * f2fs_format.c
 *
 * Copyright (c) 2012 Samsung Electronics Co., Ltd.
 *             http://www.samsung.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#define _LARGEFILE64_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <time.h>
#include <linux/fs.h>
#include <uuid/uuid.h>
#include "f2fs_fs.h"

/* chamdoo - 2013.09.18 */
/*#define ENABLE_DBG_LOG*/
#define RISA_SNAPSHOT
#define RISA_META_LOGGING
/*#define RISA_LARGE_SEGMENT*/

#ifdef ENABLE_DBG_LOG
#define dbg_log(fmt, ...)	\
	do {	\
		printf(fmt, ##__VA_ARGS__);	\
	} while (0);
#else
	#define dbg_log(fmt, ...)
#endif

#if defined(RISA_SNAPSHOT) && defined(RISA_META_LOGGING)
#define NR_SUPERBLK_SECS	1	/* # of sections for the super block */
#define NR_MAPPING_SECS		3 	/* # of sections for mapping entries */
#define NR_METALOG_TIMES	2	/* # of sections for meta-log */
unsigned long long _mapping_blkofs = 0, _mapping_byteofs = 0;
unsigned long long _meta_log_blkofs = 0, _meta_log_byteofs = 0;

struct risa_map_blk {
	__le32 magic;
	__le32 ver;
	__le32 index;
	__le32 dirty;
	__le32 mapping[F2FS_BLKSIZE/sizeof(__le32)-4];
};
#endif
/* end */

extern struct f2fs_configuration config;
struct f2fs_super_block super_block;

static void mkfs_usage()
{
	MSG(0, "\nUsage: mkfs.f2fs [options] device\n");
	MSG(0, "[options]:\n");
	MSG(0, "  -a heap-based allocation [default:1]\n");
	MSG(0, "  -d debug level [default:0]\n");
	MSG(0, "  -e [extension list] e.g. \"mp3,gif,mov\"\n");
	MSG(0, "  -l label\n");
	MSG(0, "  -o overprovision ratio [default:5]\n");
	MSG(0, "  -s # of segments per section [default:1]\n");
	MSG(0, "  -z # of sections per zone [default:1]\n");
	MSG(0, "  -t 0: nodiscard, 1: discard [default:1]\n");
	exit(1);
}

static void f2fs_parse_options(int argc, char *argv[])
{
	static const char *option_string = "a:d:e:l:o:s:z:t:";
	int32_t option=0;

	while ((option = getopt(argc,argv,option_string)) != EOF) {
		switch (option) {
		case 'a':
			config.heap = atoi(optarg);
			if (config.heap == 0)
				MSG(0, "Info: Disable heap-based policy\n");
			break;
		case 'd':
			config.dbg_lv = atoi(optarg);
			MSG(0, "Info: Debug level = %d\n", config.dbg_lv);
			break;
		case 'e':
			config.extension_list = strdup(optarg);
			MSG(0, "Info: Add new extension list\n");
			break;
		case 'l':		/*v: volume label */
			if (strlen(optarg) > 512) {
				MSG(0, "Error: Volume Label should be less than\
						512 characters\n");
				mkfs_usage();
			}
			config.vol_label = optarg;
			MSG(0, "Info: Label = %s\n", config.vol_label);
			break;
		case 'o':
			config.overprovision = atoi(optarg);
			MSG(0, "Info: Overprovision ratio = %u%%\n",
								atoi(optarg));
			break;
		case 's':
			config.segs_per_sec = atoi(optarg);
			MSG(0, "Info: Segments per section = %d\n",
								atoi(optarg));
			break;
		case 'z':
			config.secs_per_zone = atoi(optarg);
			MSG(0, "Info: Sections per zone = %d\n", atoi(optarg));
			break;
		case 't':
			config.trim = atoi(optarg);
			MSG(0, "Info: Trim is %s\n", config.trim ? "enabled": "disabled");
			break;
		default:
			MSG(0, "\tError: Unknown option %c\n",option);
			mkfs_usage();
			break;
		}
	}

	if ((optind + 1) != argc) {
		MSG(0, "\tError: Device not specified\n");
		mkfs_usage();
	}

	config.reserved_segments  =
			(2 * (100 / config.overprovision + 1) + 6)
			* config.segs_per_sec;
#ifdef RISA_SNAPSHOT
	/*config.reserved_segments  = config.segs_per_sec * (2 + 6); *//* for postmark & tpc-c */
	config.reserved_segments  = config.segs_per_sec * 2; /* for postmark & tpc-c */
#else
	config.reserved_segments  = config.segs_per_sec * 10;
#endif
	config.device_name = argv[optind];
}

const char *media_ext_lists[] = {
	"jpg",
	"gif",
	"png",
	"avi",
	"divx",
	"mp4",
	"mp3",
	"3gp",
	"wmv",
	"wma",
	"mpeg",
	"mkv",
	"mov",
	"asx",
	"asf",
	"wmx",
	"svi",
	"wvx",
	"wm",
	"mpg",
	"mpe",
	"rm",
	"ogg",
	NULL
};

static void configure_extension_list(void)
{
	const char **extlist = media_ext_lists;
	char *ext_str = config.extension_list;
	char *ue;
	int name_len;
	int i = 0;

	super_block.extension_count = 0;
	memset(super_block.extension_list, 0,
			sizeof(super_block.extension_list));

	while (*extlist) {
		name_len = strlen(*extlist);
		memcpy(super_block.extension_list[i++], *extlist, name_len);
		extlist++;
	}
	super_block.extension_count = i - 1;

	if (!ext_str)
		return;

	/* add user ext list */
	ue = strtok(ext_str, ",");
	while (ue != NULL) {
		name_len = strlen(ue);
		memcpy(super_block.extension_list[i++], ue, name_len);
		ue = strtok(NULL, ",");
		if (i > F2FS_MAX_EXTENSION)
			break;
	}

	super_block.extension_count = i - 1;

	free(config.extension_list);
}

static int f2fs_prepare_super_block(void)
{
	u_int32_t blk_size_bytes;
	u_int32_t log_sectorsize, log_sectors_per_block;
	u_int32_t log_blocksize, log_blks_per_seg;
	u_int32_t segment_size_bytes, zone_size_bytes;
	u_int32_t sit_segments;
	u_int32_t blocks_for_sit, blocks_for_nat, blocks_for_ssa;
	u_int32_t total_valid_blks_available;
	u_int64_t zone_align_start_offset, diff, total_meta_segments;
	u_int32_t sit_bitmap_size, max_nat_bitmap_size, max_nat_segments;
	u_int32_t total_zones;

#ifdef RISA_SNAPSHOT
	u_int32_t nr_meta_logging_segments = 0;
	u_int32_t nr_meta_logging_blks = 0;
#endif

	super_block.magic = cpu_to_le32(F2FS_SUPER_MAGIC);
	super_block.major_ver = cpu_to_le16(F2FS_MAJOR_VERSION);
	super_block.minor_ver = cpu_to_le16(F2FS_MINOR_VERSION);

	log_sectorsize = log_base_2(config.sector_size);
	log_sectors_per_block = log_base_2(config.sectors_per_blk);
	log_blocksize = log_sectorsize + log_sectors_per_block;
	log_blks_per_seg = log_base_2(config.blks_per_seg);

	super_block.log_sectorsize = cpu_to_le32(log_sectorsize);

	if (log_sectorsize < 0) {
		MSG(0, "\tError: Failed to get the sector size: %u!\n",
				config.sector_size);
		return -1;
	}

	super_block.log_sectors_per_block = cpu_to_le32(log_sectors_per_block);

	if (log_sectors_per_block < 0) {
		MSG(0, "\tError: Failed to get sectors per block: %u!\n",
				config.sectors_per_blk);
		return -1;
	}

	super_block.log_blocksize = cpu_to_le32(log_blocksize);
	super_block.log_blocks_per_seg = cpu_to_le32(log_blks_per_seg);

	if (log_blks_per_seg < 0) {
		MSG(0, "\tError: Failed to get block per segment: %u!\n",
				config.blks_per_seg);
		return -1;
	}

	super_block.segs_per_sec = cpu_to_le32(config.segs_per_sec);
	super_block.secs_per_zone = cpu_to_le32(config.secs_per_zone);
	blk_size_bytes = 1 << log_blocksize;
	segment_size_bytes = blk_size_bytes * config.blks_per_seg;
	zone_size_bytes =
		blk_size_bytes * config.secs_per_zone *
		config.segs_per_sec * config.blks_per_seg;

	super_block.checksum_offset = 0;

	super_block.block_count = cpu_to_le64(
		(config.total_sectors * DEFAULT_SECTOR_SIZE) /
			blk_size_bytes);

	/* chamdoo (2013.09.19) */
#ifdef RISA_SNAPSHOT
	dbg_log ("config.start_sector: %u\n"
			 "DEFAULT_SECTOR_SIZE: %u\n"
			 "F2FS_BLKSIZE: %u\n"
			 "segment_size_bytes: %u\n"
			 "zone_size_bytes: %u\n\n",
			 config.start_sector,
			 DEFAULT_SECTOR_SIZE,
			 F2FS_BLKSIZE,
			 segment_size_bytes,
			 zone_size_bytes);

	zone_align_start_offset =
		(config.start_sector * DEFAULT_SECTOR_SIZE +
		2 * F2FS_BLKSIZE + zone_size_bytes - 1) /
		zone_size_bytes * zone_size_bytes -
		config.start_sector * DEFAULT_SECTOR_SIZE;

	dbg_log ("segment0_blkaddr.org: %u\n",
		cpu_to_le32(zone_align_start_offset / blk_size_bytes));

	zone_align_start_offset =
		segment_size_bytes * cpu_to_le32(config.segs_per_sec) * 
		(NR_SUPERBLK_SECS + NR_MAPPING_SECS);	/* snapshot region */

#else
	zone_align_start_offset =
		(config.start_sector * DEFAULT_SECTOR_SIZE +
		2 * F2FS_BLKSIZE + zone_size_bytes - 1) /
		zone_size_bytes * zone_size_bytes -
		config.start_sector * DEFAULT_SECTOR_SIZE;
#endif
	/* end */

	if (config.start_sector % DEFAULT_SECTORS_PER_BLOCK) {
		MSG(0, "\tWARN: Align start sector number to the page unit\n");
		MSG(0, "\ti.e., start sector: %d, ofs:%d (sects/page: %d)\n",
				config.start_sector,
				config.start_sector % DEFAULT_SECTORS_PER_BLOCK,
				DEFAULT_SECTORS_PER_BLOCK);
	}

	super_block.segment_count = cpu_to_le32(
		((config.total_sectors * DEFAULT_SECTOR_SIZE) -
		zone_align_start_offset) / segment_size_bytes);

	super_block.segment0_blkaddr =
		cpu_to_le32(zone_align_start_offset / blk_size_bytes);
	super_block.cp_blkaddr = super_block.segment0_blkaddr;

	MSG(0, "Info: zone aligned segment0 blkaddr: %u\n",
				le32_to_cpu(super_block.segment0_blkaddr));

	super_block.segment_count_ckpt =
				cpu_to_le32(F2FS_NUMBER_OF_CHECKPOINT_PACK);

	super_block.sit_blkaddr = cpu_to_le32(
		le32_to_cpu(super_block.segment0_blkaddr) +
		(le32_to_cpu(super_block.segment_count_ckpt) *
		(1 << log_blks_per_seg)));

	blocks_for_sit = (le32_to_cpu(super_block.segment_count) +
			SIT_ENTRY_PER_BLOCK - 1) / SIT_ENTRY_PER_BLOCK;

	sit_segments = (blocks_for_sit + config.blks_per_seg - 1)
			/ config.blks_per_seg;

	super_block.segment_count_sit = cpu_to_le32(sit_segments * 2);

	super_block.nat_blkaddr = cpu_to_le32(
			le32_to_cpu(super_block.sit_blkaddr) +
			(le32_to_cpu(super_block.segment_count_sit) *
			 config.blks_per_seg));

	total_valid_blks_available = (le32_to_cpu(super_block.segment_count) -
			(le32_to_cpu(super_block.segment_count_ckpt) +
			 le32_to_cpu(super_block.segment_count_sit))) *
			config.blks_per_seg;

	blocks_for_nat = (total_valid_blks_available + NAT_ENTRY_PER_BLOCK - 1)
				/ NAT_ENTRY_PER_BLOCK;

	super_block.segment_count_nat = cpu_to_le32(
				(blocks_for_nat + config.blks_per_seg - 1) /
				config.blks_per_seg);
	/*
	 * The number of node segments should not be exceeded a "Threshold".
	 * This number resizes NAT bitmap area in a CP page.
	 * So the threshold is determined not to overflow one CP page
	 */
	sit_bitmap_size = ((le32_to_cpu(super_block.segment_count_sit) / 2) <<
				log_blks_per_seg) / 8;
	max_nat_bitmap_size = 4096 - sizeof(struct f2fs_checkpoint) + 1 -
			sit_bitmap_size;
	max_nat_segments = (max_nat_bitmap_size * 8) >> log_blks_per_seg;

	if (le32_to_cpu(super_block.segment_count_nat) > max_nat_segments)
		super_block.segment_count_nat = cpu_to_le32(max_nat_segments);

	super_block.segment_count_nat = cpu_to_le32(
			le32_to_cpu(super_block.segment_count_nat) * 2);

	super_block.ssa_blkaddr = cpu_to_le32(
			le32_to_cpu(super_block.nat_blkaddr) +
			le32_to_cpu(super_block.segment_count_nat) *
			config.blks_per_seg);

	total_valid_blks_available = (le32_to_cpu(super_block.segment_count) -
			(le32_to_cpu(super_block.segment_count_ckpt) +
			le32_to_cpu(super_block.segment_count_sit) +
			le32_to_cpu(super_block.segment_count_nat))) *
			config.blks_per_seg;

	blocks_for_ssa = total_valid_blks_available /
				config.blks_per_seg + 1;

	super_block.segment_count_ssa = cpu_to_le32(
			(blocks_for_ssa + config.blks_per_seg - 1) /
			config.blks_per_seg);

	total_meta_segments = le32_to_cpu(super_block.segment_count_ckpt) +
		le32_to_cpu(super_block.segment_count_sit) +
		le32_to_cpu(super_block.segment_count_nat) +
		le32_to_cpu(super_block.segment_count_ssa);
	diff = total_meta_segments % (config.segs_per_sec * config.secs_per_zone);

	if (diff) {
		super_block.segment_count_ssa = cpu_to_le32(
			le32_to_cpu(super_block.segment_count_ssa) +
			(config.segs_per_sec * config.secs_per_zone -
			 diff));
	}

	/* chamdoo (2013.09.19) */
#ifdef RISA_SNAPSHOT
	total_meta_segments = le32_to_cpu(super_block.segment_count_ckpt) +
		le32_to_cpu(super_block.segment_count_sit) +
		le32_to_cpu(super_block.segment_count_nat) +
		le32_to_cpu(super_block.segment_count_ssa);

	dbg_log ("total_meta_segments = %u (ckp:%u + sit:%u + nat:%u + ssa:%u)\n",
		total_meta_segments,
		le32_to_cpu(super_block.segment_count_ckpt),
		le32_to_cpu(super_block.segment_count_sit),
		le32_to_cpu(super_block.segment_count_nat),
		le32_to_cpu(super_block.segment_count_ssa));

	/* meta-log region */
	if (NR_METALOG_TIMES % 2 != 0) {
		dbg_log ("ERROR: NR_METALOG_TIMES must be even numbers = %u\n", NR_METALOG_TIMES);
		exit (-1);
	}

	nr_meta_logging_segments = total_meta_segments * (NR_METALOG_TIMES - 1);
	nr_meta_logging_blks = (nr_meta_logging_segments * config.blks_per_seg);	
	printf ("nr_meta_logging_segments: %u, nr_meta_logging_blks: %u (%u*%u = %u)\n", 
		nr_meta_logging_segments,
		nr_meta_logging_blks,
		nr_meta_logging_segments,
		config.blks_per_seg,
		nr_meta_logging_segments*config.blks_per_seg*4096
		);
#endif
	/* end */


	/* chamdoo (2013.09.19) */
#ifdef RISA_SNAPSHOT
	super_block.main_blkaddr = cpu_to_le32(
			le32_to_cpu(super_block.ssa_blkaddr) +
			(le32_to_cpu(super_block.segment_count_ssa) * config.blks_per_seg) +
			nr_meta_logging_blks); /* added for meta-logging */

	super_block.segment_count_main = cpu_to_le32(
			le32_to_cpu(super_block.segment_count) -
				(le32_to_cpu(super_block.segment_count_ckpt) + 
				 le32_to_cpu(super_block.segment_count_sit) +
			 	 le32_to_cpu(super_block.segment_count_nat) +
			 	 le32_to_cpu(super_block.segment_count_ssa) + 
			 	 nr_meta_logging_segments)	/* added for meta-logging */
			);

	super_block.section_count = cpu_to_le32(
			le32_to_cpu(super_block.segment_count_main)
			/ config.segs_per_sec);

	super_block.segment_count_main = cpu_to_le32(
			le32_to_cpu(super_block.section_count) *
			config.segs_per_sec);
#else
	super_block.main_blkaddr = cpu_to_le32(
			le32_to_cpu(super_block.ssa_blkaddr) +
			(le32_to_cpu(super_block.segment_count_ssa) * config.blks_per_seg));

	super_block.segment_count_main = cpu_to_le32(
			le32_to_cpu(super_block.segment_count) -
			(le32_to_cpu(super_block.segment_count_ckpt)
			 + le32_to_cpu(super_block.segment_count_sit) +
			 le32_to_cpu(super_block.segment_count_nat) +
			 le32_to_cpu(super_block.segment_count_ssa)));

	super_block.section_count = cpu_to_le32(
			le32_to_cpu(super_block.segment_count_main)
			/ config.segs_per_sec);

	super_block.segment_count_main = cpu_to_le32(
			le32_to_cpu(super_block.section_count) *
			config.segs_per_sec);
#endif
	/* end */

	if ((le32_to_cpu(super_block.segment_count_main) - 2) <
					config.reserved_segments) {
		MSG(0, "\tError: Device size is not sufficient for F2FS volume,\
			more segment needed =%u",
			config.reserved_segments -
			(le32_to_cpu(super_block.segment_count_main) - 2));
		return -1;
	}

	uuid_generate(super_block.uuid);

	ASCIIToUNICODE(super_block.volume_name, (u_int8_t *)config.vol_label);

	super_block.node_ino = cpu_to_le32(1);
	super_block.meta_ino = cpu_to_le32(2);
	super_block.root_ino = cpu_to_le32(3);

	total_zones = ((le32_to_cpu(super_block.segment_count_main) - 1) /
			config.segs_per_sec) /
			config.secs_per_zone;

	dbg_log ("total_zones: %u\n", total_zones);
	dbg_log ("config.segs_per_sec: %u\n", config.segs_per_sec);
	dbg_log ("config.secs_per_zone: %u\n", config.secs_per_zone);

	if (total_zones <= 6) {
		MSG(0, "\tError: %d zones: Need more zones \
			by shrinking zone size\n", total_zones);
		return -1;
	}

	if (config.heap) {
		config.cur_seg[CURSEG_HOT_NODE] = (total_zones - 1) *
					config.segs_per_sec *
					config.secs_per_zone +
					((config.secs_per_zone - 1) *
					config.segs_per_sec);

#ifdef RISA_SNAPSHOT
		dbg_log ("config.cur_seg[CURSEG_HOT_NODE](%u) = (total_zones(%u) - 1) * config.segs_per_sec(%u) * config.secs_per_zone(%u) + "
				 "((config.secs_per_zone(%u) - 1) * config.segs_per_sec(%u))",
				config.cur_seg[CURSEG_HOT_NODE],
				total_zones, 
				config.segs_per_sec, 
				config.secs_per_zone,
				config.secs_per_zone,
				config.segs_per_sec);
#endif

		config.cur_seg[CURSEG_WARM_NODE] =
					config.cur_seg[CURSEG_HOT_NODE] -
					config.segs_per_sec *
					config.secs_per_zone;
		config.cur_seg[CURSEG_COLD_NODE] =
					config.cur_seg[CURSEG_WARM_NODE] -
					config.segs_per_sec *
					config.secs_per_zone;
		config.cur_seg[CURSEG_HOT_DATA] =
					config.cur_seg[CURSEG_COLD_NODE] -
					config.segs_per_sec *
					config.secs_per_zone;
		config.cur_seg[CURSEG_COLD_DATA] = 0;
		config.cur_seg[CURSEG_WARM_DATA] =
					config.cur_seg[CURSEG_COLD_DATA] +
					config.segs_per_sec *
					config.secs_per_zone;
	} else {
		config.cur_seg[CURSEG_HOT_NODE] = 0;
		config.cur_seg[CURSEG_WARM_NODE] =
					config.cur_seg[CURSEG_HOT_NODE] +
					config.segs_per_sec *
					config.secs_per_zone;
		config.cur_seg[CURSEG_COLD_NODE] =
					config.cur_seg[CURSEG_WARM_NODE] +
					config.segs_per_sec *
					config.secs_per_zone;
		config.cur_seg[CURSEG_HOT_DATA] =
					config.cur_seg[CURSEG_COLD_NODE] +
					config.segs_per_sec *
					config.secs_per_zone;
		config.cur_seg[CURSEG_COLD_DATA] =
					config.cur_seg[CURSEG_HOT_DATA] +
					config.segs_per_sec *
					config.secs_per_zone;
		config.cur_seg[CURSEG_WARM_DATA] =
					config.cur_seg[CURSEG_COLD_DATA] +
					config.segs_per_sec *
					config.secs_per_zone;
	}

	configure_extension_list();

	return 0;
}

static int f2fs_init_sit_area(void)
{
	u_int32_t blk_size, seg_size;
	u_int32_t index = 0;
	u_int64_t sit_seg_addr = 0;
	u_int8_t *zero_buf = NULL;

	blk_size = 1 << le32_to_cpu(super_block.log_blocksize);
	seg_size = (1 << le32_to_cpu(super_block.log_blocks_per_seg)) *
							blk_size;

	zero_buf = calloc(sizeof(u_int8_t), seg_size);
	if(zero_buf == NULL) {
		MSG(0, "\tError: Calloc Failed for sit_zero_buf!!!\n");
		return -1;
	}

	sit_seg_addr = le32_to_cpu(super_block.sit_blkaddr);
	sit_seg_addr *= blk_size;

#ifdef RISA_SNAPSHOT
	dbg_log ("\n----\n");
	dbg_log ("sit_start_addr: super_block.sit_blkaddr: %lu (%lu)\n", 
			le32_to_cpu(super_block.sit_blkaddr), 
			le32_to_cpu(super_block.sit_blkaddr * blk_size));

	dbg_log ("sit_length: super_block.segment_count_sit: %d (%llu)\n", 
			super_block.segment_count_sit,
			super_block.segment_count_sit * seg_size);
#endif

	for (index = 0;
		index < (le32_to_cpu(super_block.segment_count_sit) / 2);
								index++) {
#ifndef RISA_SNAPSHOT
		if (dev_write(zero_buf, sit_seg_addr, seg_size)) {
			MSG(0, "\tError: While zeroing out the sit area \
					on disk!!!\n");
			return -1;
		}
#endif
		sit_seg_addr += seg_size;
	}

	free(zero_buf);
	return 0 ;
}

static int f2fs_init_nat_area(void)
{
	u_int32_t blk_size, seg_size;
	u_int32_t index = 0;
	u_int64_t nat_seg_addr = 0;
	u_int8_t *nat_buf = NULL;

	blk_size = 1 << le32_to_cpu(super_block.log_blocksize);
	seg_size = (1 << le32_to_cpu(super_block.log_blocks_per_seg)) *
							blk_size;

	nat_buf = calloc(sizeof(u_int8_t), seg_size);
	if (nat_buf == NULL) {
		MSG(0, "\tError: Calloc Failed for nat_zero_blk!!!\n");
		return -1;
	}

	nat_seg_addr = le32_to_cpu(super_block.nat_blkaddr);
	nat_seg_addr *= blk_size;

#ifdef RISA_SNAPSHOT
	dbg_log ("\n----\n");
	dbg_log ("nat_start_addr: super_block.nat_blkaddr: %lu (%lu)\n", 
			le32_to_cpu(super_block.nat_blkaddr), 
			le32_to_cpu(super_block.nat_blkaddr * blk_size));

	dbg_log ("nat_length: super_block.segment_count_nat: %d (%llu)\n", 
			super_block.segment_count_nat,
			super_block.segment_count_nat * seg_size);
#endif

	for (index = 0;
		index < (le32_to_cpu(super_block.segment_count_nat) / 2);
								index++) {
#ifndef RISA_SNAPSHOT
		if (dev_write(nat_buf, nat_seg_addr, seg_size)) {
			MSG(0, "\tError: While zeroing out the nat area \
					on disk!!!\n");
			return -1;
		}
#endif
		nat_seg_addr = nat_seg_addr + (2 * seg_size);
	}

	free(nat_buf);
	return 0 ;
}

static int f2fs_write_check_point_pack(void)
{
	struct f2fs_checkpoint *ckp = NULL;
	struct f2fs_summary_block *sum = NULL;
	u_int32_t blk_size_bytes;
	u_int64_t cp_seg_blk_offset = 0;
	u_int32_t crc = 0;
	int i;

	ckp = calloc(F2FS_BLKSIZE, 1);
	if (ckp == NULL) {
		MSG(0, "\tError: Calloc Failed for f2fs_checkpoint!!!\n");
		return -1;
	}

	sum = calloc(F2FS_BLKSIZE, 1);
	if (sum == NULL) {
		MSG(0, "\tError: Calloc Failed for summay_node!!!\n");
		return -1;
	}

	/* 1. cp page 1 of checkpoint pack 1 */
	ckp->checkpoint_ver = 1;
	ckp->cur_node_segno[0] =
		cpu_to_le32(config.cur_seg[CURSEG_HOT_NODE]);
	ckp->cur_node_segno[1] =
		cpu_to_le32(config.cur_seg[CURSEG_WARM_NODE]);
	ckp->cur_node_segno[2] =
		cpu_to_le32(config.cur_seg[CURSEG_COLD_NODE]);
	ckp->cur_data_segno[0] =
		cpu_to_le32(config.cur_seg[CURSEG_HOT_DATA]);
	ckp->cur_data_segno[1] =
		cpu_to_le32(config.cur_seg[CURSEG_WARM_DATA]);
	ckp->cur_data_segno[2] =
		cpu_to_le32(config.cur_seg[CURSEG_COLD_DATA]);
	for (i = 3; i < MAX_ACTIVE_NODE_LOGS; i++) {
		ckp->cur_node_segno[i] = 0xffffffff;
		ckp->cur_data_segno[i] = 0xffffffff;
	}

	ckp->cur_node_blkoff[0] = cpu_to_le16(1);
	ckp->cur_data_blkoff[0] = cpu_to_le16(1);
	ckp->valid_block_count = cpu_to_le64(2);
	ckp->rsvd_segment_count = cpu_to_le32(config.reserved_segments);
	ckp->overprov_segment_count = cpu_to_le32(
			(le32_to_cpu(super_block.segment_count_main) -
			le32_to_cpu(ckp->rsvd_segment_count)) *
			config.overprovision / 100);
	ckp->overprov_segment_count = cpu_to_le32(
			le32_to_cpu(ckp->overprov_segment_count) +
			le32_to_cpu(ckp->rsvd_segment_count));

	/* main segments - reserved segments - (node + data segments) */
	ckp->free_segment_count = cpu_to_le32(
			le32_to_cpu(super_block.segment_count_main) - 6);
	ckp->user_block_count = cpu_to_le64(
			((le32_to_cpu(ckp->free_segment_count) + 6 -
			le32_to_cpu(ckp->overprov_segment_count)) *
			 config.blks_per_seg));
	ckp->cp_pack_total_block_count = cpu_to_le32(8);
	ckp->ckpt_flags |= CP_UMOUNT_FLAG;
	ckp->cp_pack_start_sum = cpu_to_le32(1);
	ckp->valid_node_count = cpu_to_le32(1);
	ckp->valid_inode_count = cpu_to_le32(1);
	ckp->next_free_nid = cpu_to_le32(
			le32_to_cpu(super_block.root_ino) + 1);

	ckp->sit_ver_bitmap_bytesize = cpu_to_le32(
			((le32_to_cpu(super_block.segment_count_sit) / 2) <<
			 le32_to_cpu(super_block.log_blocks_per_seg)) / 8);

	ckp->nat_ver_bitmap_bytesize = cpu_to_le32(
			((le32_to_cpu(super_block.segment_count_nat) / 2) <<
			 le32_to_cpu(super_block.log_blocks_per_seg)) / 8);

	ckp->checksum_offset = cpu_to_le32(4092);

	crc = f2fs_cal_crc32(F2FS_SUPER_MAGIC, ckp,
					le32_to_cpu(ckp->checksum_offset));
	*((u_int32_t *)((unsigned char *)ckp +
				le32_to_cpu(ckp->checksum_offset))) = crc;

	blk_size_bytes = 1 << le32_to_cpu(super_block.log_blocksize);
	cp_seg_blk_offset = le32_to_cpu(super_block.segment0_blkaddr);
	cp_seg_blk_offset *= blk_size_bytes;

#if defined(RISA_SNAPSHOT) && defined(RISA_META_LOGGING)
	dbg_log ("\n----\n");
	dbg_log ("ckp0_start_addr: %u (%u)\n", 
		le32_to_cpu(super_block.segment0_blkaddr),
		le32_to_cpu(super_block.segment0_blkaddr) * blk_size_bytes);

	dbg_log ("[W] ckp0:\t" "%" PRIu64 "\t%u\t" "%" PRIu64 "\n", _meta_log_byteofs/F2FS_BLKSIZE, F2FS_BLKSIZE/F2FS_BLKSIZE, (cp_seg_blk_offset + F2FS_BLKSIZE)/F2FS_BLKSIZE);

	dbg_log ("%llu => %llu\n", _meta_log_byteofs/F2FS_BLKSIZE, cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(ckp, _meta_log_byteofs, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the ckp to disk!!!\n");
		return -1;
	}
	_meta_log_byteofs += F2FS_BLKSIZE;
#else
	dbg_log ("%llu\n", cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(ckp, cp_seg_blk_offset, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the ckp to disk!!!\n");
		return -1;
	}
#endif

	/* 2. Prepare and write Segment summary for data blocks */
	memset(sum, 0, sizeof(struct f2fs_summary_block));
	SET_SUM_TYPE((&sum->footer), SUM_TYPE_DATA);

	sum->entries[0].nid = super_block.root_ino;
	sum->entries[0].ofs_in_node = 0;

	cp_seg_blk_offset += blk_size_bytes;
#if defined(RISA_SNAPSHOT) && defined(RISA_META_LOGGING)
	dbg_log ("[W] sum_blk:\t" "%" PRIu64 "\t%u\t" "%" PRIu64 "\n", _meta_log_byteofs/F2FS_BLKSIZE, F2FS_BLKSIZE/F2FS_BLKSIZE, (cp_seg_blk_offset + F2FS_BLKSIZE)/F2FS_BLKSIZE);
	dbg_log ("%llu => %llu\n", _meta_log_byteofs/F2FS_BLKSIZE, cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(sum, _meta_log_byteofs, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the sum_blk to disk!!!\n");
		return -1;
	}
	_meta_log_byteofs += F2FS_BLKSIZE;
#else
	dbg_log ("%llu\n", cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(sum, cp_seg_blk_offset, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the sum_blk to disk!!!\n");
		return -1;
	}
#endif

	/* 3. Fill segment summary for data block to zero. */
	memset(sum, 0, sizeof(struct f2fs_summary_block));
	SET_SUM_TYPE((&sum->footer), SUM_TYPE_DATA);

	cp_seg_blk_offset += blk_size_bytes;
#if defined(RISA_SNAPSHOT) && defined(RISA_META_LOGGING)
	dbg_log ("[W] sum_blk:\t" "%" PRIu64 "\t%u\t" "%" PRIu64 "\n", _meta_log_byteofs/F2FS_BLKSIZE, F2FS_BLKSIZE/F2FS_BLKSIZE, (cp_seg_blk_offset + F2FS_BLKSIZE)/F2FS_BLKSIZE);
	dbg_log ("%llu => %llu\n", _meta_log_byteofs/F2FS_BLKSIZE, cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(sum, _meta_log_byteofs, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the sum_blk to disk!!!\n");
		return -1;
	}
	_meta_log_byteofs += F2FS_BLKSIZE;
#else
	dbg_log ("%llu\n", cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(sum, cp_seg_blk_offset, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the sum_blk to disk!!!\n");
		return -1;
	}
#endif

	/* 4. Fill segment summary for data block to zero. */
	memset(sum, 0, sizeof(struct f2fs_summary_block));
	SET_SUM_TYPE((&sum->footer), SUM_TYPE_DATA);

	/* inode sit for root */
	sum->n_sits = cpu_to_le16(6);
	sum->sit_j.entries[0].segno = ckp->cur_node_segno[0];
	sum->sit_j.entries[0].se.vblocks = cpu_to_le16((CURSEG_HOT_NODE << 10) | 1);
	f2fs_set_bit(0, sum->sit_j.entries[0].se.valid_map);
	sum->sit_j.entries[1].segno = ckp->cur_node_segno[1];
	sum->sit_j.entries[1].se.vblocks = cpu_to_le16((CURSEG_WARM_NODE << 10));
	sum->sit_j.entries[2].segno = ckp->cur_node_segno[2];
	sum->sit_j.entries[2].se.vblocks = cpu_to_le16((CURSEG_COLD_NODE << 10));

	/* data sit for root */
	sum->sit_j.entries[3].segno = ckp->cur_data_segno[0];
	sum->sit_j.entries[3].se.vblocks = cpu_to_le16((CURSEG_HOT_DATA << 10) | 1);
	f2fs_set_bit(0, sum->sit_j.entries[3].se.valid_map);
	sum->sit_j.entries[4].segno = ckp->cur_data_segno[1];
	sum->sit_j.entries[4].se.vblocks = cpu_to_le16((CURSEG_WARM_DATA << 10));
	sum->sit_j.entries[5].segno = ckp->cur_data_segno[2];
	sum->sit_j.entries[5].se.vblocks = cpu_to_le16((CURSEG_COLD_DATA << 10));

	cp_seg_blk_offset += blk_size_bytes;
#if defined(RISA_SNAPSHOT) && defined(RISA_META_LOGGING)
	dbg_log ("[W] sum_blk:\t" "%" PRIu64 "\t%u\t" "%" PRIu64 "\n", _meta_log_byteofs/F2FS_BLKSIZE, F2FS_BLKSIZE/F2FS_BLKSIZE, (cp_seg_blk_offset + F2FS_BLKSIZE)/F2FS_BLKSIZE);
	dbg_log ("%llu => %llu\n", _meta_log_byteofs/F2FS_BLKSIZE, cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(sum, _meta_log_byteofs, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the sum_blk to disk!!!\n");
		return -1;
	}
	_meta_log_byteofs += F2FS_BLKSIZE;
#else
	dbg_log ("%llu\n", cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(sum, cp_seg_blk_offset, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the sum_blk to disk!!!\n");
		return -1;
	}
#endif

	/* 5. Prepare and write Segment summary for node blocks */
	memset(sum, 0, sizeof(struct f2fs_summary_block));
	SET_SUM_TYPE((&sum->footer), SUM_TYPE_NODE);

	sum->entries[0].nid = super_block.root_ino;
	sum->entries[0].ofs_in_node = 0;

	cp_seg_blk_offset += blk_size_bytes;
#if defined(RISA_SNAPSHOT) && defined(RISA_META_LOGGING)
	dbg_log ("[W] sum_blk:\t" "%" PRIu64 "\t%u\t" "%" PRIu64 "\n", _meta_log_byteofs/F2FS_BLKSIZE, F2FS_BLKSIZE/F2FS_BLKSIZE, (cp_seg_blk_offset + F2FS_BLKSIZE)/F2FS_BLKSIZE);
	dbg_log ("%llu => %llu\n", _meta_log_byteofs/F2FS_BLKSIZE, cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(sum, _meta_log_byteofs, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the sum_blk to disk!!!\n");
		return -1;
	}
	_meta_log_byteofs += F2FS_BLKSIZE;
#else
	dbg_log ("%llu\n", cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(sum, cp_seg_blk_offset, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the sum_blk to disk!!!\n");
		return -1;
	}
#endif

	/* 6. Fill segment summary for data block to zero. */
	memset(sum, 0, sizeof(struct f2fs_summary_block));
	SET_SUM_TYPE((&sum->footer), SUM_TYPE_NODE);

	cp_seg_blk_offset += blk_size_bytes;
#if defined(RISA_SNAPSHOT) && defined(RISA_META_LOGGING)
	dbg_log ("[W] sum_blk:\t" "%" PRIu64 "\t%u\t" "%" PRIu64 "\n", _meta_log_byteofs/F2FS_BLKSIZE, F2FS_BLKSIZE/F2FS_BLKSIZE, (cp_seg_blk_offset + F2FS_BLKSIZE)/F2FS_BLKSIZE);
	dbg_log ("%llu => %llu\n", _meta_log_byteofs/F2FS_BLKSIZE, cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(sum, _meta_log_byteofs, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the sum_blk to disk!!!\n");
		return -1;
	}
	_meta_log_byteofs += F2FS_BLKSIZE;
#else
	dbg_log ("%llu\n", cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(sum, cp_seg_blk_offset, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the sum_blk to disk!!!\n");
		return -1;
	}
#endif

	/* 7. Fill segment summary for data block to zero. */
	memset(sum, 0, sizeof(struct f2fs_summary_block));
	SET_SUM_TYPE((&sum->footer), SUM_TYPE_NODE);
	cp_seg_blk_offset += blk_size_bytes;
#if defined(RISA_SNAPSHOT) && defined(RISA_META_LOGGING)
	dbg_log ("[W] sum_blk:\t" "%" PRIu64 "\t%u\t" "%" PRIu64 "\n", _meta_log_byteofs/F2FS_BLKSIZE, F2FS_BLKSIZE/F2FS_BLKSIZE, (cp_seg_blk_offset + F2FS_BLKSIZE)/F2FS_BLKSIZE);
	dbg_log ("%llu => %llu\n", _meta_log_byteofs/F2FS_BLKSIZE, cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(sum, _meta_log_byteofs, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the sum_blk to disk!!!\n");
		return -1;
	}
	_meta_log_byteofs += F2FS_BLKSIZE;
#else
	dbg_log ("%llu\n", cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(sum, cp_seg_blk_offset, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the sum_blk to disk!!!\n");
		return -1;
	}
#endif

	/* 8. cp page2 */
	cp_seg_blk_offset += blk_size_bytes;
#if defined(RISA_SNAPSHOT) && defined(RISA_META_LOGGING)
	dbg_log ("[W] ckp0:\t" "%" PRIu64 "\t%u\t" "%" PRIu64 "\n", _meta_log_byteofs/F2FS_BLKSIZE, F2FS_BLKSIZE/F2FS_BLKSIZE, (cp_seg_blk_offset + F2FS_BLKSIZE)/F2FS_BLKSIZE);
	dbg_log ("%llu => %llu\n", _meta_log_byteofs/F2FS_BLKSIZE, cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(ckp, _meta_log_byteofs, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the ckp to disk!!!\n");
		return -1;
	}
	_meta_log_byteofs += F2FS_BLKSIZE;
#else
	dbg_log ("%llu\n", cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(ckp, cp_seg_blk_offset, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the ckp to disk!!!\n");
		return -1;
	}
#endif

	/* 9. cp page 1 of check point pack 2
	 * Initiatialize other checkpoint pack with version zero
	 */
	ckp->checkpoint_ver = 0;

	crc = f2fs_cal_crc32(F2FS_SUPER_MAGIC, ckp,
					le32_to_cpu(ckp->checksum_offset));
	*((u_int32_t *)((unsigned char *)ckp +
				le32_to_cpu(ckp->checksum_offset))) = crc;

	cp_seg_blk_offset = (le32_to_cpu(super_block.segment0_blkaddr) +
				config.blks_per_seg) *
				blk_size_bytes;

#if defined(RISA_SNAPSHOT) && defined(RISA_META_LOGGING)
	dbg_log ("\n----\n");
	dbg_log ("ckp1_start_addr: %u (%u)\n", 
			(le32_to_cpu(super_block.segment0_blkaddr) + config.blks_per_seg),
			(le32_to_cpu(super_block.segment0_blkaddr) + config.blks_per_seg) * blk_size_bytes);

	dbg_log ("[W] ckp1:\t" "%" PRIu64 "\t%u\t" "%" PRIu64 "\n", _meta_log_byteofs/F2FS_BLKSIZE, F2FS_BLKSIZE/F2FS_BLKSIZE, (cp_seg_blk_offset + F2FS_BLKSIZE)/F2FS_BLKSIZE);
	dbg_log ("%llu => %llu\n", _meta_log_byteofs/F2FS_BLKSIZE, cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(ckp, _meta_log_byteofs, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the ckp to disk!!!\n");
		return -1;
	}
	_meta_log_byteofs += F2FS_BLKSIZE;
#else
	dbg_log ("%llu\n", cp_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(ckp, cp_seg_blk_offset, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the ckp to disk!!!\n");
		return -1;
	}
#endif

	free(sum);
	free(ckp);
	return	0;
}

static int f2fs_write_super_block(void)
{
	int index;
	u_int8_t *zero_buff;

	zero_buff = calloc(F2FS_BLKSIZE, 1);

	memcpy(zero_buff + F2FS_SUPER_OFFSET, &super_block,
						sizeof(super_block));

#ifdef RISA_SNAPSHOT
	dbg_log ("\n----\n");
	dbg_log ("superblock_start_addr: %u\n", 0);

	for (index = 0; index < 2; index++) {
		dbg_log ("[W] super_blk: %u\t%u\t%u\n", (index * F2FS_BLKSIZE)/F2FS_BLKSIZE, F2FS_BLKSIZE/F2FS_BLKSIZE, (index * F2FS_BLKSIZE + F2FS_BLKSIZE)/F2FS_BLKSIZE);
		if (dev_write(zero_buff, index * F2FS_BLKSIZE, F2FS_BLKSIZE)) {
			MSG(0, "\tError: While while writing supe_blk \
					on disk!!! index : %d\n", index);
			return -1;
		}
	}
#else
	for (index = 0; index < 2; index++) {
		if (dev_write(zero_buff, index * F2FS_BLKSIZE, F2FS_BLKSIZE)) {
			MSG(0, "\tError: While while writing supe_blk \
					on disk!!! index : %d\n", index);
			return -1;
		}
	}
#endif

	free(zero_buff);
	return 0;
}

static int f2fs_write_root_inode(void)
{
	struct f2fs_node *raw_node = NULL;
	u_int64_t blk_size_bytes, data_blk_nor;
	u_int64_t main_area_node_seg_blk_offset = 0;

	raw_node = calloc(F2FS_BLKSIZE, 1);
	if (raw_node == NULL) {
		MSG(0, "\tError: Calloc Failed for raw_node!!!\n");
		return -1;
	}

	raw_node->footer.nid = super_block.root_ino;
	raw_node->footer.ino = super_block.root_ino;
	raw_node->footer.cp_ver = cpu_to_le64(1);
	raw_node->footer.next_blkaddr = cpu_to_le32(
			le32_to_cpu(super_block.main_blkaddr) +
			config.cur_seg[CURSEG_HOT_NODE] *
			config.blks_per_seg + 1);

	raw_node->i.i_mode = cpu_to_le16(0x41ed);
	raw_node->i.i_links = cpu_to_le32(2);
	raw_node->i.i_uid = cpu_to_le32(getuid());
	raw_node->i.i_gid = cpu_to_le32(getgid());

	blk_size_bytes = 1 << le32_to_cpu(super_block.log_blocksize);
	raw_node->i.i_size = cpu_to_le64(1 * blk_size_bytes); /* dentry */
	raw_node->i.i_blocks = cpu_to_le64(2);

	raw_node->i.i_atime = cpu_to_le32(time(NULL));
	raw_node->i.i_atime_nsec = 0;
	raw_node->i.i_ctime = cpu_to_le32(time(NULL));
	raw_node->i.i_ctime_nsec = 0;
	raw_node->i.i_mtime = cpu_to_le32(time(NULL));
	raw_node->i.i_mtime_nsec = 0;
	raw_node->i.i_generation = 0;
	raw_node->i.i_xattr_nid = 0;
	raw_node->i.i_flags = 0;
	raw_node->i.i_current_depth = cpu_to_le32(1);

	data_blk_nor = le32_to_cpu(super_block.main_blkaddr) +
		config.cur_seg[CURSEG_HOT_DATA] * config.blks_per_seg;
	raw_node->i.i_addr[0] = cpu_to_le32(data_blk_nor);

	raw_node->i.i_ext.fofs = 0;
	raw_node->i.i_ext.blk_addr = cpu_to_le32(data_blk_nor);
	raw_node->i.i_ext.len = cpu_to_le32(1);

	main_area_node_seg_blk_offset = le32_to_cpu(super_block.main_blkaddr);
	main_area_node_seg_blk_offset += config.cur_seg[CURSEG_HOT_NODE] *
					config.blks_per_seg;
	main_area_node_seg_blk_offset *= blk_size_bytes;

#if 1
#ifndef RISA_SNAPSHOT
	dbg_log ("\n----\n");
	dbg_log ("main_start_addr: %u (%u)\n", 
		le32_to_cpu(super_block.main_blkaddr),
		le32_to_cpu(super_block.main_blkaddr) * blk_size_bytes);
	dbg_log (" * hot_node_start_addr: %u(=%u+%u*%u) (%u)\n",
		(le32_to_cpu(super_block.main_blkaddr) + config.cur_seg[CURSEG_HOT_NODE] * config.blks_per_seg),
		le32_to_cpu(super_block.main_blkaddr),
		config.cur_seg[CURSEG_HOT_NODE],
		config.blks_per_seg,
		(le32_to_cpu(super_block.main_blkaddr) + config.cur_seg[CURSEG_HOT_NODE] * config.blks_per_seg) * blk_size_bytes);

	dbg_log ("[W] raw_node:\t" "%" PRIu64 "\t%u\t" "%" PRIu64 "\n", 
		main_area_node_seg_blk_offset/F2FS_BLKSIZE, 
		F2FS_BLKSIZE/F2FS_BLKSIZE, 
		(main_area_node_seg_blk_offset + F2FS_BLKSIZE)/F2FS_BLKSIZE);
#endif
#endif

	if (dev_write(raw_node, main_area_node_seg_blk_offset, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the raw_node to disk!!!\n");
		return -1;
	}

#ifndef RISA_SNAPSHOT
	memset(raw_node, 0xff, sizeof(struct f2fs_node));

	main_area_node_seg_blk_offset += F2FS_BLKSIZE;
	if (dev_write(raw_node, main_area_node_seg_blk_offset, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the raw_node to disk!!!\n");
		return -1;
	}
#endif

	free(raw_node);
	return 0;
}

static int f2fs_update_nat_root(void)
{
	struct f2fs_nat_block *nat_blk = NULL;
	u_int64_t blk_size_bytes, nat_seg_blk_offset = 0;

	nat_blk = calloc(F2FS_BLKSIZE, 1);
	if(nat_blk == NULL) {
		MSG(0, "\tError: Calloc Failed for nat_blk!!!\n");
		return -1;
	}

	/* update root */
#ifdef RISA_SNAPSHOT
	dbg_log ("\n----\n");
	dbg_log ("super_block.root_ino = %u\n", super_block.root_ino);
#endif
	nat_blk->entries[super_block.root_ino].block_addr = cpu_to_le32(
		le32_to_cpu(super_block.main_blkaddr) +
		config.cur_seg[CURSEG_HOT_NODE] * config.blks_per_seg);
	nat_blk->entries[super_block.root_ino].ino = super_block.root_ino;

	/* update node nat */
#ifdef RISA_SNAPSHOT
	dbg_log ("super_block.node_ino = %u\n", super_block.node_ino);
#endif
	nat_blk->entries[super_block.node_ino].block_addr = cpu_to_le32(1);
	nat_blk->entries[super_block.node_ino].ino = super_block.node_ino;

	/* update meta nat */
#ifdef RISA_SNAPSHOT
	dbg_log ("super_block.meta_ino = %u\n", super_block.meta_ino);
#endif
	nat_blk->entries[super_block.meta_ino].block_addr = cpu_to_le32(1);
	nat_blk->entries[super_block.meta_ino].ino = super_block.meta_ino;

	blk_size_bytes = 1 << le32_to_cpu(super_block.log_blocksize);
	nat_seg_blk_offset = le32_to_cpu(super_block.nat_blkaddr);
	nat_seg_blk_offset *= blk_size_bytes;

#if defined(RISA_SNAPSHOT) && defined(RISA_META_LOGGING)
	dbg_log ("[W] nat_blk_set0:\t" "%" PRIu64 "\t%u\t" "%" PRIu64 "\n", _meta_log_byteofs/F2FS_BLKSIZE, F2FS_BLKSIZE/F2FS_BLKSIZE, (nat_seg_blk_offset + F2FS_BLKSIZE)/F2FS_BLKSIZE);
	dbg_log ("%llu => %llu\n", _meta_log_byteofs/F2FS_BLKSIZE, nat_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(nat_blk, _meta_log_byteofs, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the nat_blk set0 to disk!\n");
		return -1;
	}
	_meta_log_byteofs += F2FS_BLKSIZE;
#else
	dbg_log ("%llu\n", nat_seg_blk_offset/F2FS_BLKSIZE);
	if (dev_write(nat_blk, nat_seg_blk_offset, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the nat_blk set0 to disk!\n");
		return -1;
	}
#endif

	free(nat_blk);
	return 0;
}

static int f2fs_add_default_dentry_root(void)
{
	struct f2fs_dentry_block *dent_blk = NULL;
	u_int64_t blk_size_bytes, data_blk_offset = 0;

	dent_blk = calloc(F2FS_BLKSIZE, 1);
	if(dent_blk == NULL) {
		MSG(0, "\tError: Calloc Failed for dent_blk!!!\n");
		return -1;
	}

	dent_blk->dentry[0].hash_code = 0;
	dent_blk->dentry[0].ino = super_block.root_ino;
	dent_blk->dentry[0].name_len = cpu_to_le16(1);
	dent_blk->dentry[0].file_type = F2FS_FT_DIR;
	memcpy(dent_blk->filename[0], ".", 1);

	dent_blk->dentry[1].hash_code = 0;
	dent_blk->dentry[1].ino = super_block.root_ino;
	dent_blk->dentry[1].name_len = cpu_to_le16(2);
	dent_blk->dentry[1].file_type = F2FS_FT_DIR;
	memcpy(dent_blk->filename[1], "..", 2);

	/* bitmap for . and .. */
	dent_blk->dentry_bitmap[0] = (1 << 1) | (1 << 0);
	blk_size_bytes = 1 << le32_to_cpu(super_block.log_blocksize);
	data_blk_offset = le32_to_cpu(super_block.main_blkaddr);
	data_blk_offset += config.cur_seg[CURSEG_HOT_DATA] *
				config.blks_per_seg;
	data_blk_offset *= blk_size_bytes;

#ifdef RISA_SNAPSHOT
	dbg_log ("\n----\n");
	dbg_log ("main_start_addr: %u (%u)\n",
		le32_to_cpu(super_block.main_blkaddr),
		le32_to_cpu(super_block.main_blkaddr) * blk_size_bytes);
	dbg_log (" * hot_data_start_addr: %u (%u)\n",
		(le32_to_cpu(super_block.main_blkaddr) + config.cur_seg[CURSEG_HOT_DATA] * config.blks_per_seg),
		(le32_to_cpu(super_block.main_blkaddr) + config.cur_seg[CURSEG_HOT_DATA] * config.blks_per_seg) * blk_size_bytes);

	dbg_log ("[W] dentry_blk:\t" "%" PRIu64 "\t%u\t" "%" PRIu64 "\n", data_blk_offset/F2FS_BLKSIZE, F2FS_BLKSIZE/F2FS_BLKSIZE, (data_blk_offset + F2FS_BLKSIZE)/F2FS_BLKSIZE);
#endif
	if (dev_write(dent_blk, data_blk_offset, F2FS_BLKSIZE)) {
		MSG(0, "\tError: While writing the dentry_blk to disk!!!\n");
		return -1;
	}

	free(dent_blk);
	return 0;
}

static int f2fs_create_root_dir(void)
{
	int err = 0;

	err = f2fs_write_root_inode();
	if (err < 0) {
		MSG(0, "\tError: Failed to write root inode!!!\n");
		goto exit;
	}

	err = f2fs_update_nat_root();
	if (err < 0) {
		MSG(0, "\tError: Failed to update NAT for root!!!\n");
		goto exit;
	}

	err = f2fs_add_default_dentry_root();
	if (err < 0) {
		MSG(0, "\tError: Failed to add default dentries for root!!!\n");
		goto exit;
	}
exit:
	if (err)
		MSG(0, "\tError: Could not create the root directory!!!\n");

	return err;
}

int f2fs_trim_device()
{
	unsigned long long range[2];
	struct stat stat_buf;

	if (!config.trim)
		return 0;

	range[0] = 0;
	range[1] = config.total_sectors * DEFAULT_SECTOR_SIZE;

	if (fstat(config.fd, &stat_buf) < 0 ) {
		MSG(0, "\tError: Failed to get the device stat!!!\n");
		return -1;
	}

	if (S_ISREG(stat_buf.st_mode))
		return 0;
	else if (S_ISBLK(stat_buf.st_mode)) {
		if (ioctl(config.fd, BLKDISCARD, &range) < 0)
			MSG(0, "Info: This device doesn't support TRIM\n");
	} else
		return -1;
	return 0;
}

#if 0
#ifdef RISA_SNAPSHOT
int f2fs_write_snapshot(void)
{
	__le32* ptr_mapping_table = NULL;
	u_int8_t* ptr_mapping_summary_table = NULL;
	u_int32_t nr_segment_count_meta = 0;
	u_int32_t nr_mapping_entries = 0;
	u_int32_t nr_mapping_summary_entries = 0;

	u_int32_t index = 0;
	u_int32_t segment0_blkaddr = 0;

	/* (1) obtain the number of mapping entries */
	nr_segment_count_meta = le32_to_cpu(super_block.segment_count_ckpt) + 
		le32_to_cpu(super_block.segment_count_sit) + 
		le32_to_cpu(super_block.segment_count_nat) +
		le32_to_cpu(super_block.segment_count_ssa);

	nr_mapping_entries = nr_segment_count_meta * config.blks_per_seg;
	nr_mapping_entries += 2;	/* one for start position and the other for end position */

	nr_mapping_summary_entries = 
		nr_segment_count_meta * NR_METALOG_TIMES *
		config.blks_per_seg;

	dbg_log ("\n----\n");
	dbg_log ("nr_segment_count_meta: %u\n", nr_segment_count_meta);
	dbg_log ("nr_mapping_entries: %u (%u Bytes)\n", nr_mapping_entries, nr_mapping_entries * sizeof (__le32));
	dbg_log ("nr_mapping_summary_entries: %u (%u Bytes)\n", nr_mapping_summary_entries, nr_mapping_summary_entries * sizeof (u_int8_t));

	/* (2) create the mapping table & summary table */
	ptr_mapping_table = (__le32*)malloc (sizeof (__le32) * nr_mapping_entries);
	if (ptr_mapping_table == NULL) {
		dbg_log ("\tError: errors occur allocating memory space for the mapping table\n");
		return -1;
	}
	ptr_mapping_summary_table = (u_int8_t*)malloc (sizeof (u_int8_t) * nr_mapping_summary_entries);
	if (ptr_mapping_summary_table == NULL) {
		dbg_log ("\tError: errors occur allocating memory space for the mapping table summary\n");
		return -1;
	}

	/* (3) initialize the mapping table */
	segment0_blkaddr = le32_to_cpu (super_block.segment0_blkaddr);
	dbg_log ("segment0_blkaddr: %u\n", segment0_blkaddr);

	/* set all entries to free */
	for (index = 0; index < nr_mapping_entries; index++) {
		ptr_mapping_table[index] = cpu_to_le32(0);
	}
	for (index = 0; index < nr_mapping_summary_entries; index++) {
		ptr_mapping_summary_table[index] = cpu_to_le32(0);	/* free: 0, valid: 1, invalid: 2 */
	}

	/* set some default entries (logical <= physical) */
#if defined(RISA_SNAPSHOT) && defined(RISA_META_LOGGING)
	{
		unsigned long long ofs = _meta_log_blkofs;

		ptr_mapping_table[ofs+2048-segment0_blkaddr] = cpu_to_le32(ofs+0);
		ptr_mapping_table[ofs+0-segment0_blkaddr] = cpu_to_le32(ofs+1);
		ptr_mapping_table[ofs+1-segment0_blkaddr] = cpu_to_le32(ofs+2);
		ptr_mapping_table[ofs+2-segment0_blkaddr] = cpu_to_le32(ofs+3);
		ptr_mapping_table[ofs+3-segment0_blkaddr] = cpu_to_le32(ofs+4);
		ptr_mapping_table[ofs+4-segment0_blkaddr] = cpu_to_le32(ofs+5);
		ptr_mapping_table[ofs+5-segment0_blkaddr] = cpu_to_le32(ofs+6);
		ptr_mapping_table[ofs+6-segment0_blkaddr] = cpu_to_le32(ofs+7);
		ptr_mapping_table[ofs+7-segment0_blkaddr] = cpu_to_le32(ofs+8);
		ptr_mapping_table[ofs+512-segment0_blkaddr] = cpu_to_le32(ofs+9);

		ptr_mapping_table[nr_mapping_entries-2] = cpu_to_le32(0);	// start position
		ptr_mapping_table[nr_mapping_entries-1] = cpu_to_le32(10);	// end position

		ptr_mapping_summary_table[ofs+0-segment0_blkaddr] = 1;
		ptr_mapping_summary_table[ofs+1-segment0_blkaddr] = 1;
		ptr_mapping_summary_table[ofs+2-segment0_blkaddr] = 1;
		ptr_mapping_summary_table[ofs+3-segment0_blkaddr] = 1;
		ptr_mapping_summary_table[ofs+4-segment0_blkaddr] = 1;
		ptr_mapping_summary_table[ofs+5-segment0_blkaddr] = 1;
		ptr_mapping_summary_table[ofs+6-segment0_blkaddr] = 1;
		ptr_mapping_summary_table[ofs+7-segment0_blkaddr] = 1;
		ptr_mapping_summary_table[ofs+8-segment0_blkaddr] = 1;
		ptr_mapping_summary_table[ofs+9-segment0_blkaddr] = 1;
	}
#else
	printf ("ORIGINAL\n");
	ptr_mapping_table[1024-segment0_blkaddr] = cpu_to_le32(1024);
	ptr_mapping_table[1025-segment0_blkaddr] = cpu_to_le32(1025);
	ptr_mapping_table[1026-segment0_blkaddr] = cpu_to_le32(1026);
	ptr_mapping_table[1027-segment0_blkaddr] = cpu_to_le32(1027);
	ptr_mapping_table[1028-segment0_blkaddr] = cpu_to_le32(1028);
	ptr_mapping_table[1029-segment0_blkaddr] = cpu_to_le32(1029);
	ptr_mapping_table[1030-segment0_blkaddr] = cpu_to_le32(1030);
	ptr_mapping_table[1031-segment0_blkaddr] = cpu_to_le32(1031);
	ptr_mapping_table[1536-segment0_blkaddr] = cpu_to_le32(1536);
	ptr_mapping_table[3072-segment0_blkaddr] = cpu_to_le32(3072);

	ptr_mapping_table[nr_mapping_entries-2] = cpu_to_le32(0);	/* start position */
	ptr_mapping_table[nr_mapping_entries-1] = cpu_to_le32(3072);	/* end position */

	ptr_mapping_summary_table[1024-segment0_blkaddr] = 1;
	ptr_mapping_summary_table[1025-segment0_blkaddr] = 1;
	ptr_mapping_summary_table[1026-segment0_blkaddr] = 1;
	ptr_mapping_summary_table[1027-segment0_blkaddr] = 1;
	ptr_mapping_summary_table[1028-segment0_blkaddr] = 1;
	ptr_mapping_summary_table[1029-segment0_blkaddr] = 1;
	ptr_mapping_summary_table[1030-segment0_blkaddr] = 1;
	ptr_mapping_summary_table[1031-segment0_blkaddr] = 1;
	ptr_mapping_summary_table[1536-segment0_blkaddr] = 1;
	ptr_mapping_summary_table[3072-segment0_blkaddr] = 1;
#endif

	_mapping_blkofs = (config.segs_per_sec * config.blks_per_seg);

	/* (4) write the mapping table & summary table */
	dbg_log ("[W] mapping_table:\t%u\t%u\n",
		_mapping_blkofs, (sizeof (__le32) * nr_mapping_entries + F2FS_BLKSIZE - 1) / F2FS_BLKSIZE);

	if (dev_write(ptr_mapping_table, _mapping_blkofs * F2FS_BLKSIZE, sizeof (__le32) * nr_mapping_entries)) {
		MSG(0, "\tError: While writing the mapping table to disk!!!\n");
		return -1;
	}

	dbg_log ("[W] mapping_summary_table:\t%u\t%u\n", 
		_mapping_blkofs + (sizeof (__le32) * nr_mapping_entries + F2FS_BLKSIZE - 1) / F2FS_BLKSIZE, 
		(sizeof (u_int8_t) * nr_mapping_summary_entries + F2FS_BLKSIZE - 1) / F2FS_BLKSIZE);

	if (dev_write(ptr_mapping_summary_table, 
		_mapping_blkofs * F2FS_BLKSIZE + ((sizeof (__le32) * nr_mapping_entries + F2FS_BLKSIZE - 1) / F2FS_BLKSIZE) * F2FS_BLKSIZE,
		sizeof (u_int8_t) * nr_mapping_summary_entries)) {
		MSG(0, "\tError: While writing the mapping summary table to disk!!!\n");
		return -1;
	}

	/* (5) free the memory space */
	free (ptr_mapping_summary_table);
	free (ptr_mapping_table);

	return 0;
}
#endif
#endif

#ifdef RISA_SNAPSHOT
int f2fs_write_snapshot(void)
{
	/*__le32* ptr_mapping_table = NULL;*/
	u_int32_t nr_segment_count_meta = 0;
	u_int32_t nr_mapping_entries = 0;
	u_int32_t nr_map_blks = 0;

	u_int32_t loop = 0;
	u_int32_t segment0_blkaddr = 0;

	struct risa_map_blk* ptr_map_blks;

	/* (1) obtain the number of mapping entries */
	nr_segment_count_meta = 
		le32_to_cpu(super_block.segment_count_ckpt) + 
		le32_to_cpu(super_block.segment_count_sit) + 
		le32_to_cpu(super_block.segment_count_nat) +
		le32_to_cpu(super_block.segment_count_ssa);

	nr_mapping_entries = 
		nr_segment_count_meta * config.blks_per_seg;

	nr_map_blks = nr_mapping_entries / 1020;
	if (nr_mapping_entries % 1020 != 0) {
		nr_map_blks++;
	}


	dbg_log ("\n----\n");
	dbg_log ("nr_segment_count_meta: %u\n", nr_segment_count_meta);
	dbg_log ("nr_mapping_entries: %u (%u Bytes)\n", nr_mapping_entries, nr_mapping_entries * sizeof (__le32));
	dbg_log ("nr_map_blks: %u\n", nr_map_blks);

	/* (2) create the mapping table */
	if ((ptr_map_blks = (struct risa_map_blk*)malloc (
			sizeof (struct risa_map_blk) * nr_map_blks)) == NULL) {
		dbg_log ("\tError: errors occur allocating memory space for the map-blk table\n");
		return -1;
	} 
	for (loop = 0; loop < nr_map_blks; loop++) {
		ptr_map_blks[loop].magic = cpu_to_le32 (0xEF);
		ptr_map_blks[loop].ver = cpu_to_le32(0);
		ptr_map_blks[loop].index = cpu_to_le32 (loop * 1020);
		/*ptr_map_blks[loop].len = cpu_to_le32(1020);*/
		ptr_map_blks[loop].dirty = cpu_to_le32(0);
		memset (ptr_map_blks[loop].mapping, (__le32)-1, sizeof (__le32) * 1020);
	}

	/* (3) initialize the mapping table */
	segment0_blkaddr = le32_to_cpu (super_block.segment0_blkaddr);
	dbg_log ("segment0_blkaddr: %u\n", segment0_blkaddr);

	/* set some default entries (logical <= physical) */
#if defined(RISA_SNAPSHOT) && defined(RISA_META_LOGGING)
	{
		unsigned long long ofs = _meta_log_blkofs;
		unsigned long long map_ofs;

		map_ofs = ofs+2048-segment0_blkaddr;
		ptr_map_blks[map_ofs/1020].mapping[map_ofs%1020] = cpu_to_le32(ofs+0);
		map_ofs = ofs+0-segment0_blkaddr;
		ptr_map_blks[map_ofs/1020].mapping[map_ofs%1020] = cpu_to_le32(ofs+1);
		map_ofs = ofs+1-segment0_blkaddr;
		ptr_map_blks[map_ofs/1020].mapping[map_ofs%1020] = cpu_to_le32(ofs+2);
		map_ofs = ofs+2-segment0_blkaddr;
		ptr_map_blks[map_ofs/1020].mapping[map_ofs%1020] = cpu_to_le32(ofs+3);
		map_ofs = ofs+3-segment0_blkaddr;
		ptr_map_blks[map_ofs/1020].mapping[map_ofs%1020] = cpu_to_le32(ofs+4);
		map_ofs = ofs+4-segment0_blkaddr;
		ptr_map_blks[map_ofs/1020].mapping[map_ofs%1020] = cpu_to_le32(ofs+5);
		map_ofs = ofs+5-segment0_blkaddr;
		ptr_map_blks[map_ofs/1020].mapping[map_ofs%1020] = cpu_to_le32(ofs+6);
		map_ofs = ofs+6-segment0_blkaddr;
		ptr_map_blks[map_ofs/1020].mapping[map_ofs%1020] = cpu_to_le32(ofs+7);
		map_ofs = ofs+7-segment0_blkaddr;
		ptr_map_blks[map_ofs/1020].mapping[map_ofs%1020] = cpu_to_le32(ofs+8);
		map_ofs = ofs+512-segment0_blkaddr;
		ptr_map_blks[map_ofs/1020].mapping[map_ofs%1020] = cpu_to_le32(ofs+9);

		//ptr_mapping_table[ofs+2048-segment0_blkaddr] = cpu_to_le32(ofs+0);
		//ptr_mapping_table[ofs+0-segment0_blkaddr] = cpu_to_le32(ofs+1);
		//ptr_mapping_table[ofs+1-segment0_blkaddr] = cpu_to_le32(ofs+2);
		//ptr_mapping_table[ofs+2-segment0_blkaddr] = cpu_to_le32(ofs+3);
		//ptr_mapping_table[ofs+3-segment0_blkaddr] = cpu_to_le32(ofs+4);
		/*ptr_mapping_table[ofs+4-segment0_blkaddr] = cpu_to_le32(ofs+5);*/
		//ptr_mapping_table[ofs+5-segment0_blkaddr] = cpu_to_le32(ofs+6);
		/*ptr_mapping_table[ofs+6-segment0_blkaddr] = cpu_to_le32(ofs+7);*/
		/*ptr_mapping_table[ofs+7-segment0_blkaddr] = cpu_to_le32(ofs+8);*/
		/*ptr_mapping_table[ofs+512-segment0_blkaddr] = cpu_to_le32(ofs+9);*/
	}
#else
	/*
	printf ("ORIGINAL\n");
	ptr_mapping_table[1024-segment0_blkaddr] = cpu_to_le32(1024);
	ptr_mapping_table[1025-segment0_blkaddr] = cpu_to_le32(1025);
	ptr_mapping_table[1026-segment0_blkaddr] = cpu_to_le32(1026);
	ptr_mapping_table[1027-segment0_blkaddr] = cpu_to_le32(1027);
	ptr_mapping_table[1028-segment0_blkaddr] = cpu_to_le32(1028);
	ptr_mapping_table[1029-segment0_blkaddr] = cpu_to_le32(1029);
	ptr_mapping_table[1030-segment0_blkaddr] = cpu_to_le32(1030);
	ptr_mapping_table[1031-segment0_blkaddr] = cpu_to_le32(1031);
	ptr_mapping_table[1536-segment0_blkaddr] = cpu_to_le32(1536);
	ptr_mapping_table[3072-segment0_blkaddr] = cpu_to_le32(3072);
	*/
#endif

	/* (4) write the mapping table & summary table */
	_mapping_blkofs = ((config.segs_per_sec * config.blks_per_seg) * NR_SUPERBLK_SECS) * F2FS_BLKSIZE; 
	for (loop = 0; loop < nr_map_blks; loop++) {
		dbg_log ("[W] mapping_table:\t%u\t%u\n", _mapping_blkofs/F2FS_BLKSIZE, F2FS_BLKSIZE);
		if (dev_write ((__le32*)(ptr_map_blks + loop), _mapping_blkofs, F2FS_BLKSIZE)) {
			MSG(0, "\tError: While writing the mapping table to disk!!!\n");
			return -1;
		}
		_mapping_blkofs += F2FS_BLKSIZE;
	}
	/*
	dbg_log ("[W] mapping_table:\t%u\t%u\n",
		_mapping_blkofs, (sizeof (__le32) * nr_mapping_entries + F2FS_BLKSIZE - 1) / F2FS_BLKSIZE);

	if (dev_write(ptr_mapping_table, _mapping_blkofs * F2FS_BLKSIZE, sizeof (__le32) * nr_mapping_entries)) {
		MSG(0, "\tError: While writing the mapping table to disk!!!\n");
		return -1;
	}
	*/

	/* (5) free the memory space */
	free (ptr_map_blks);
	/*free (ptr_mapping_table);*/

	return 0;
}
#endif


static int f2fs_format_device(void)
{
	int err = 0;

	err= f2fs_prepare_super_block();
	if (err < 0) {
		MSG(0, "\tError: Failed to prepare a super block!!!\n");
		goto exit;
	}

	err = f2fs_trim_device();
	if (err < 0) {
		MSG(0, "\tError: Failed to trim whole device!!!\n");
		goto exit;
	}

	err = f2fs_init_sit_area();
	if (err < 0) {
		MSG(0, "\tError: Failed to Initialise the SIT AREA!!!\n");
		goto exit;
	}

	err = f2fs_init_nat_area();
	if (err < 0) {
		MSG(0, "\tError: Failed to Initialise the NAT AREA!!!\n");
		goto exit;
	}

	err = f2fs_create_root_dir();
	if (err < 0) {
		MSG(0, "\tError: Failed to create the root directory!!!\n");
		goto exit;
	}

	err = f2fs_write_check_point_pack();
	if (err < 0) {
		MSG(0, "\tError: Failed to write the check point pack!!!\n");
		goto exit;
	}

	err = f2fs_write_super_block();
	if (err < 0) {
		MSG(0, "\tError: Failed to write the Super Block!!!\n");
		goto exit;
	}

/*#if defined(RISA_SNAPSHOT) || defined(RISA_META_LOGGING)*/
#ifdef RISA_SNAPSHOT
	err = f2fs_write_snapshot();
	if (err < 0) {
		MSG(0, "\tError: Failed to write the snapshot!!!\n");
		goto exit;
	}
#endif

exit:
	if (err)
		MSG(0, "\tError: Could not format the device!!!\n");

	/*
	 * We should call fsync() to flush out all the dirty pages
	 * in the block device page cache.
	 */
	if (fsync(config.fd) < 0)
		MSG(0, "\tError: Could not conduct fsync!!!\n");

	if (close(config.fd) < 0)
		MSG(0, "\tError: Failed to close device file!!!\n");

	return err;
}

int main(int argc, char *argv[])
{
	MSG(0, "\n\tF2FS-tools: mkfs.f2fs Ver: %s (%s)\n\n",
				F2FS_TOOLS_VERSION,
				F2FS_TOOLS_DATE);

#ifdef RISA_SNAPSHOT
	MSG(0, "\n\tModified for RISA (RC.1) (%s %s)\n\n", __TIME__, __DATE__);
#endif

	f2fs_init_configuration(&config);

	f2fs_parse_options(argc, argv);

	if (f2fs_dev_is_mounted(&config) < 0)
		return -1;

	if (f2fs_get_device_info(&config) < 0)
		return -1;

	/* modified by chamdoo */
	config.reserved_segments = (config.total_sectors / 4 * 0.05) / (2 * 1024);
	MSG (0, "\t %d = %f KB / %d KB\n",
		config.reserved_segments,
		config.total_sectors / 4 * 0.05,
		2 * 1024);

	if (config.reserved_segments % config.segs_per_sec != 0) {
		config.reserved_segments += config.segs_per_sec;
		config.reserved_segments /= config.segs_per_sec;
		config.reserved_segments *= config.segs_per_sec;
	}
	MSG (0, "\tINFO: overprovisioning = %d segs (%d MB)\n", 
		config.reserved_segments,
		config.reserved_segments * 2);
	/* end */

#if defined(RISA_SNAPSHOT) && defined(RISA_META_LOGGING)
	/* For RISA, two sectors are reserved for check points.
	 * The metalog begins after two check points */
	_meta_log_blkofs = 
		(config.segs_per_sec * config.blks_per_seg) * 
		(NR_SUPERBLK_SECS + NR_MAPPING_SECS);
	_meta_log_byteofs = _meta_log_blkofs * F2FS_BLKSIZE;
	dbg_log ("_meta_log_blkofs = %llu (B)\n", _meta_log_blkofs);
	dbg_log ("_meta_log_byteofs = %llu (B)\n", _meta_log_byteofs);
#endif

	if (f2fs_format_device() < 0)
		return -1;

	MSG(0, "Info: format successful\n");

	return 0;
}
